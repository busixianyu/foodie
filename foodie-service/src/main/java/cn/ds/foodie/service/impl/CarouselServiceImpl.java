package cn.ds.foodie.service.impl;

import cn.ds.foodie.pojo.Carousel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import cn.ds.foodie.mapper.CarouselMapper;
import cn.ds.foodie.service.CarouselService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author laona
 * @description 轮播图实现类
 * @create 2022-05-08 13:58
 **/
@Service
public class CarouselServiceImpl implements CarouselService {

    @Resource
    private CarouselMapper carouselMapper;

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Exception.class)
    @Override
    public List<Carousel> queryAll(Integer isShow) {

        Example example = new Example(Carousel.class);
        // 降序
        example.orderBy("sort").desc();
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("isShow", isShow);

        List<Carousel> carouselList = carouselMapper.selectByExample(example);

        return carouselList;
    }
}
