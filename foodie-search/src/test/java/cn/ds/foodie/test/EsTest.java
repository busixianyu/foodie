package cn.ds.foodie.test;

import cn.ds.foodie.SearchApplication;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.util.CollectionUtils;
import cn.ds.foodie.es.pojo.Stu;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author laona
 * @decription es测试类
 * @since 2022-06-20 20:06
 **/
@Slf4j
@SpringBootTest(classes = SearchApplication.class)
public class EsTest {
    /**
     * 不建议使用 ElasticsearchTemplate 对索引进行管理（创建索引、更新映射、删除索引）
     * 索引等价于数据表，不建议在业务中直接操作索引
     * 只会对其中的数据进行CRUD
     */
    @Resource
    private ElasticsearchTemplate esTemplate;

    @Test
    public void createIndexStu() {

        Stu stu = new Stu();
        stu.setAge(24);
        stu.setName("laona");
        stu.setStuId(10002L);

        IndexQuery indexQuery = new IndexQueryBuilder().withObject(stu).build();
        String result = esTemplate.index(indexQuery);
        Assertions.assertNotNull(result, "插入失败");
    }

    @Test
    public void updateIndexStu() {

        Stu stu = new Stu();
        stu.setAge(24);
        stu.setName("laona");
        stu.setStuId(10002L);
        stu.setMoney(18.8f);
        stu.setSign("i am spider man!");
        stu.setDescription("i am not super man");

        IndexQuery indexQuery = new IndexQueryBuilder().withObject(stu).build();
        String result = esTemplate.index(indexQuery);
        Assertions.assertNotNull(result, "更新失败");
    }

    @Test
    public void deleteIndexStu() {

        boolean b = esTemplate.deleteIndex(Stu.class);
        Assertions.assertTrue(b, "删除失败");
    }

    // ==========================对文档进行CURD============================
    @Test
    public void updateStuDoc() {

        Map<String, Object> sourceMap = new HashMap<>();
        sourceMap.put("sign", "我不是超人");
        sourceMap.put("money", 88.6f);
        sourceMap.put("age", 33);

        IndexRequest indexRequest = new IndexRequest();
        indexRequest.source(sourceMap);

        // update stu set sign='我不是超人', age=33, money=88.6 where stuId = '10002'
        UpdateQuery updateQuery = new UpdateQueryBuilder()
                .withClass(Stu.class)
                // id 是文档中自动生成的id
                .withId("10002")
                .withIndexRequest(indexRequest)
                .build();
        UpdateResponse update = esTemplate.update(updateQuery);

        Assertions.assertNotNull(update, "更新文档失败");
    }

    @Test
    public void getIndexStuDoc() {
        GetQuery getQuery = new GetQuery();
        getQuery.setId("10002");

        Stu stu = esTemplate.queryForObject(getQuery, Stu.class);
        log.info("stu : {} ", stu);

        Assertions.assertNotNull(stu, "更新文档失败");
    }

    @Test
    public void deleteIndexStuDoc() {

        String result = esTemplate.delete(Stu.class, "10002");

        Assertions.assertNotNull(result, "删除文档失败");
    }

    // ==========================对文档进行检索操作============================

    @Test
    public void queryPageStuDoc() {

        Pageable pageable = PageRequest.of(0, 10);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("description", "man"))
                .withPageable(pageable)
                .build();


        AggregatedPage<Stu> pagedStu = esTemplate.queryForPage(searchQuery, Stu.class);
        log.info("pagedStu: {}", pagedStu.getContent());

        Assertions.assertNotNull(pagedStu, "查询文档失败");
    }


    @Test
    public void highLightStuDoc() {

        String preTag = "<front color='red'>";
        String postTag = "</front>";

        Pageable pageable = PageRequest.of(0, 10);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("description", "man"))
                // 高亮标签
                .withHighlightFields(
                        new HighlightBuilder.Field("description")
                                .preTags(preTag)
                                .postTags(postTag)
                )
                // 排序: money降序
                .withSort(
                        SortBuilders.fieldSort("money")
                                .order(SortOrder.DESC)
                )
                // 排序: age升序
                .withSort(
                        SortBuilders.fieldSort("age")
                                .order(SortOrder.ASC)
                )
                // 分页
                .withPageable(pageable)
                .build();

        AggregatedPage<Stu> pagedStu = esTemplate.queryForPage(searchQuery, Stu.class, new SearchResultMapper() {
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse resp, Class<T> aClass, Pageable pageable) {

                List<Stu> stuList = new ArrayList<>();

                // 处理高亮的内容
                SearchHits hits = resp.getHits();
                for (SearchHit hit : hits) {
                    HighlightField highlightField = hit.getHighlightFields().get("description");
                    String highlightVal = highlightField.getFragments()[0].toString();

                    Stu highlightTarget = new Stu();
                    Map<String, Object> source = hit.getSourceAsMap();
                    highlightTarget.setDescription(highlightVal);
                    highlightTarget.setStuId(Long.valueOf(source.get("stuId").toString()));
                    highlightTarget.setName(((String) source.get("name")));
                    highlightTarget.setMoney(Float.valueOf(source.get("money").toString()));
                    highlightTarget.setAge(((Integer) source.get("age")));
                    highlightTarget.setSign(((String) source.get("sign")));

                    stuList.add(highlightTarget);
                }

                if (CollectionUtils.isEmpty(stuList)) {
                    return null;
                }

                return new AggregatedPageImpl<>((List<T>) stuList);
            }

            @Override
            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {

                return null;
            }
        });
        log.info("pagedStu: {}", pagedStu.getContent());

        Assertions.assertNotNull(pagedStu, "查询文档失败");
    }
}
