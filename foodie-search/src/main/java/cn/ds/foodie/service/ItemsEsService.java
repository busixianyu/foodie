package cn.ds.foodie.service;

import cn.ds.foodie.utils.PagedGridResult;

/**
 * @author laona
 * @decription items ES
 * @since 2022-06-25 15:54
 **/
public interface ItemsEsService {

    /**
     * 查找商品信息
     *
     * @param keywords 搜索关键字
     * @param sort     分类
     * @param page     当前页码
     * @param pageSize 每页数量
     * @return 商品信息
     */
    public PagedGridResult searchItems(String keywords, String sort, Integer page, Integer pageSize);
}
