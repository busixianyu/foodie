package cn.ds.foodie;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * @author laona
 * @decription
 * @since 2022-06-19 14:33
 **/
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class SearchApplication {

    public static void main(String[] args) {
        try {
            ConfigurableApplicationContext application = SpringApplication.run(SearchApplication.class, args);
            Environment env = application.getEnvironment();
            String ip = InetAddress.getLocalHost().getHostAddress();
            String port = env.getProperty("server.port");
            String path = env.getProperty("server.servlet.context-path");
            if (StringUtils.isBlank(path)) {
                path = "";
            }
            System.out.println(
                    "\n----------------------------------------------------------\n\t" +
                            "Application Jeecg-Boot is running! Access URLs:\n\t" +
                            "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                            "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                            "前端地址: \thttp://localhost:8080/foodie-shop/\n\t" +
                            "Swagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n" +
                            "----------------------------------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
