package cn.ds.foodie.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author laona
 * @description hello controller
 * @date 2022-04-26 19:55
 **/
@ApiIgnore
@Api(value = "Hello 测试", tags = {"用于测试的相关接口"})
@RestController
public class HelloController {

    final static Logger logger = LoggerFactory.getLogger(HelloController.class);

    @ApiOperation(value = "测试系统连通性", tags = {"用户测试springboot启动情况"}, httpMethod = "GET")
    @GetMapping("/hello")
    public String hello() {

        logger.debug("debug: hello~");
        logger.info("info: hello~");
        logger.warn("warn: hello~");
        logger.error("error: hello~");
        return "hello world!";
    }

}
