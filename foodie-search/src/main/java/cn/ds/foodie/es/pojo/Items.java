package cn.ds.foodie.es.pojo;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


/**
 * @author laona
 * @decription 商品列表
 * @since 2022-06-25 15:36
 **/
@Data
@ToString
@Document(indexName = "foodie-items-ik", type = "doc", createIndex = false)
public class Items {

    @Id
    @Field(store = true, type = FieldType.Text, index = false)
    private String itemId;

    @Field(store = true, type = FieldType.Text, index = true)
    private String itemName;

    @Field(store = true, type = FieldType.Text, index = false)
    private String imgUrl;

    @Field(store = true, type = FieldType.Integer, index = true)
    private Integer price;

    @Field(store = true, type = FieldType.Integer, index = true)
    private Integer sellCounts;
}
