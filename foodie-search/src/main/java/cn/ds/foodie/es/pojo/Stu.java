package cn.ds.foodie.es.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


/**
 * @author laona
 * @decription 学生类
 * @since 2022-06-20 20:08
 **/
@Data
@Document(indexName = "stu", type = "doc", createIndex = false)
public class Stu {

    @Id
    private Long stuId;

    @Field(store = true)
    private String name;

    @Field(store = true)
    private Integer age;

    @Field(store = true)
    private String description;

    @Field(store = true, type = FieldType.Keyword)
    private String sign;

    @Field(store = true)
    private Float money;
}
