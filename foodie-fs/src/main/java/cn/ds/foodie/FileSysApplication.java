package cn.ds.foodie;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import tk.mybatis.spring.annotation.MapperScan;

import java.net.InetAddress;

/**
 * @author laona
 * @decription 文件服务器启动类
 * @since 2022-06-29 21:08
 **/
@Slf4j
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@MapperScan(basePackages = {"wiki.laona.mapper"})
@ComponentScan(basePackages = {"wiki.laona", "org.n3r.idworker"})
@EnableScheduling
@EnableRedisHttpSession     // 开启使用redis作为spring session
public class FileSysApplication {

    public static void main(String[] args) {
        try {
            ConfigurableApplicationContext application = SpringApplication.run(FileSysApplication.class, args);
            Environment env = application.getEnvironment();
            String ip = InetAddress.getLocalHost().getHostAddress();
            String port = env.getProperty("server.port");
            String path = env.getProperty("server.servlet.context-path");
            if (StringUtils.isBlank(path)) {
                path = "";
            }

            System.out.println(
                    "\n----------------------------------------------------------\n\t" +
                            "Application Jeecg-Boot is running! Access URLs:\n\t" +
                            "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                            "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                            "Swagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n" +
                            "----------------------------------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
