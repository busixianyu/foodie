package cn.ds.foodie.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author laona
 * @decription oss
 * @since 2022-07-03 15:32
 **/
public interface IOssService {

    /**
     * 文件上传
     *
     * @param file        文件
     * @param userId      用户id
     * @param fileExtName 文件扩展名
     * @return 文件上传路径
     * @throws IOException io异常
     */
    String upload(MultipartFile file, String userId, String fileExtName) throws IOException;
}
