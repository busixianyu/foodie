package cn.ds.foodie.service;

import cn.ds.foodie.constant.EnumFileType;

import java.io.InputStream;

/**
 * @author laona
 * @decription minio 接口
 * @since 2022-06-29 21:22
 **/
public interface IMinioService {

    /**
     * 上传服务
     *
     * @param fileName 文件名
     * @param input    上传的文件流
     * @param fileType {@link EnumFileType} 文件类型
     * @return 上传路径
     * @throws Exception 异常
     */
    String upload(String fileName, InputStream input, EnumFileType fileType) throws Exception;
}
