package cn.ds.foodie.config;

import cn.ds.foodie.constant.MinioProp;
import io.minio.MinioClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author laona
 * @decription minio 配置
 * @since 2022-06-29 21:25
 **/
@Configuration
public class MinioConfiguration {

    @Resource
    private MinioProp minioProp;

    @Bean
    public MinioClient minioClient() {

        return MinioClient.builder()
                .endpoint(minioProp.getEndpoint())
                .credentials(minioProp.getAccessKey(), minioProp.getSecretKey())
                .build();
    }
}
