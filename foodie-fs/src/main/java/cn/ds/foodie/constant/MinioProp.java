package cn.ds.foodie.constant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author laona
 * @decriptiConnio配置
 * @since 2022-06-29 21:19
 **/
@Data
@Component
@ConfigurationProperties(value ="minio")
public class MinioProp {
    /**
     * 访问地址
     */
    private String endpoint;
    /**
     * key
     */
    private String accessKey;
    /**
     * 秘钥
     */
    private String secretKey;
}
