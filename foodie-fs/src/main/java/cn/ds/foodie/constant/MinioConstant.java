package cn.ds.foodie.constant;

/**
 * @author laona
 * @decription minio常量
 * @since 2022-06-29 21:33
 **/
public interface MinioConstant {

    /**
     * 图片存放路径
     */
    String IMAGES_BUCKET_NAME = "images";
    /**
     * 其他文件存放路径
     */
    String DEFAULT_BUCKET_NAME = "others";
}
