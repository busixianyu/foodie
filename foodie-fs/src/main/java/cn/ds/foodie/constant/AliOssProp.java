package cn.ds.foodie.constant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author laona
 * @decription 阿里云oss配置
 * @since 2022-07-03 15:17
 **/
@Data
@Component
@ConfigurationProperties(prefix = "ali-oss")
public class AliOssProp {

    /**
     * 外网访问
     */
    private String endPoint;
    /**
     * key
     */
    private String accessKeyId;
    /**
     * secret
     */
    private String accessKeySecret;
    /**
     * bucketName
     */
    private String bucketName;
    /**
     * 文件夹名
     */
    private String objectName;
    /**
     * bucket访问域名
     */
    private String bucketEndPoint;
}
