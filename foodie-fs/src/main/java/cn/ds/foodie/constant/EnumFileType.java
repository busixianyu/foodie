package cn.ds.foodie.constant;

/**
 * @author laona
 * @decription 文件类型
 * @since 2022-06-29 21:36
 **/
public enum EnumFileType {

    /**
     * 图片
     */
    IMAGE("image"),

    OTHER("other")
    ;

    /**
     * 文件类型
     */
    public final String type;

    EnumFileType(String type) {
        this.type = type;
    }
}
