package cn.ds.foodie.service.impl;

import cn.ds.foodie.constant.EnumFileType;
import cn.ds.foodie.service.IMinioService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.nio.file.Files;

/**
 * @author laona
 * @decription 测试
 * @since 2022-06-30 21:57
 **/
@Slf4j
@SpringBootTest
public class MinioServiceTest {

    @Resource
    private IMinioService minioService;

    @Test
    public void testUpload() throws Exception {

        final String fileName = "xm.jpeg";
        final String filePath = "/Users/laona/Desktop/";

        String result = minioService.upload(fileName,
                                            Files.newInputStream(new File(filePath + fileName).toPath()),
                                            EnumFileType.IMAGE);
        Assertions.assertNotNull(result);

        log.info("result:{}", result);
    }

}
