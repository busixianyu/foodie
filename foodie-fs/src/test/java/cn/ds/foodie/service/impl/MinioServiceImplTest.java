package cn.ds.foodie.service.impl;

import cn.ds.foodie.constant.EnumFileType;
import io.minio.*;
import okhttp3.Headers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MinioServiceImplTest {

    @Mock
    private MinioClient mockMinioClient;

    @InjectMocks
    private MinioServiceImpl minioServiceImplUnderTest;

    @Test
    void testUpload() throws Exception {
        // Setup
        final InputStream stream = new ByteArrayInputStream("content".getBytes());
        when(mockMinioClient.bucketExists(new BucketExistsArgs())).thenReturn(false);

        // Configure MinioClient.putObject(...).
        final ObjectWriteResponse objectWriteResponse = new ObjectWriteResponse(Headers.of("namesAndValues"), "bucket",
                "region", "object", "etag", "versionId");
        when(mockMinioClient.putObject(new PutObjectArgs())).thenReturn(objectWriteResponse);

        // Run the test
        final String result = minioServiceImplUnderTest.upload("fileName", stream, EnumFileType.IMAGE);

        // Verify the results
        assertThat(result).isEqualTo("result");
        verify(mockMinioClient).makeBucket(new MakeBucketArgs());
    }
    //
    // @Test
    // void testUpload_EmptyStream() throws Exception {
    //     // Setup
    //     final InputStream stream = new NullInputStream();
    //     when(mockMinioClient.bucketExists(new BucketExistsArgs())).thenReturn(false);
    //
    //     // Configure MinioClient.putObject(...).
    //     final ObjectWriteResponse objectWriteResponse = new ObjectWriteResponse(Headers.of("namesAndValues"), "bucket",
    //             "region", "object", "etag", "versionId");
    //     when(mockMinioClient.putObject(new PutObjectArgs())).thenReturn(objectWriteResponse);
    //
    //     // Run the test
    //     final String result = minioServiceImplUnderTest.upload("fileName", stream, EnumFileType.IMAGE);
    //
    //     // Verify the results
    //     assertThat(result).isEqualTo("result");
    //     verify(mockMinioClient).makeBucket(new MakeBucketArgs());
    // }
}
