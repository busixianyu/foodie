package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.ItemsImg;

public interface ItemsImgMapper extends MyMapper<ItemsImg> {
}