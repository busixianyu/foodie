package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.OrderStatus;

public interface OrderStatusMapper extends MyMapper<OrderStatus> {
}