package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.Items;

public interface ItemsMapper extends MyMapper<Items> {
}