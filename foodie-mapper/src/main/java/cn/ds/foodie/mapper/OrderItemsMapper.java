package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.OrderItems;

public interface OrderItemsMapper extends MyMapper<OrderItems> {
}