package cn.ds.foodie.mapper;

import org.springframework.stereotype.Repository;
import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.Users;

@Repository(value = "usersMapper")
public interface UsersMapper extends MyMapper<Users> {
}