package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.UserAddress;

public interface UserAddressMapper extends MyMapper<UserAddress> {
}