package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.ItemsParam;

public interface ItemsParamMapper extends MyMapper<ItemsParam> {
}