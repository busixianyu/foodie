package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.Orders;

public interface OrdersMapper extends MyMapper<Orders> {
}