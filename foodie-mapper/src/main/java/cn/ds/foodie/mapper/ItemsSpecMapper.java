package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.ItemsSpec;

public interface ItemsSpecMapper extends MyMapper<ItemsSpec> {
}