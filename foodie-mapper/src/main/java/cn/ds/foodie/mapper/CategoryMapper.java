package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.Category;

public interface CategoryMapper extends MyMapper<Category> {
}