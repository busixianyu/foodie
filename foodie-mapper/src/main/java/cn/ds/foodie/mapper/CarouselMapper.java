package cn.ds.foodie.mapper;

import cn.ds.foodie.my.mapper.MyMapper;
import cn.ds.foodie.pojo.Carousel;

public interface CarouselMapper extends MyMapper<Carousel> {
}