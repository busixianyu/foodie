package cn.ds.foodie.pojo.vo;

import cn.ds.foodie.pojo.bo.ShopcartBO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author laona
 * @description 订单VO
 * @since 2022-05-11 23:44
 **/
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OrderVO {

    private String orderId;
    private MerchantOrdersVO merchantOrdersVO;
    /**
     * 待删除购物车列表
     */
    private List<ShopcartBO> shopcartList;
}
